### Descripción del proyecto
Desarrollo de proyecto para entrevista el cual se desarrollo en laravel 8 y vue como spa
El sistema cuenta con validacion tanto del frontend como del backend
Se utilizó jwt para la autenticacion en el front

### Instrucciones
* Clonar el repositorio
git clone https://victor_azuara@bitbucket.org/victor_azuara/entrevista3hbc.git

* Usando la linea de comandos, entrar a la carpeta que se creo y descargar las librerias requeridas por el sistem
  + composer install
  + npm install

* Copiar el archivo .env.example a .env
* Llenar los datos de conexion a la base de datos segun sea su caso

* ejecutar los siguientes comandos
  + php artisan key:gen
  + php artisan migrate --seed (esto generara datos de prueba, asi como los usuarios admin y operator)
  + npm run build

* debe ejecutar el comando 
  + php artisan serve

lo anterior hara que se ejecute un servidor de pruebas en el puerto 8000 y podra acceder al sistema 
a traves de http://localhost:8000/login

 
