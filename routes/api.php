<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\AirportsController;
use App\Http\Controllers\AirlinesController;
use App\Http\Controllers\FlightsController;
use App\Http\Controllers\FrontController;

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('userinfo', [FrontController::class, 'userinfo']);
    Route::get('airports', [AirportsController::class, 'index']);
    Route::get('airports/combo', [AirportsController::class, 'combo']);
    Route::delete('airports', [AirportsController::class, 'delete']);
    Route::post('airports', [AirportsController::class, 'store']);
    Route::put('airports', [AirportsController::class, 'update']);
    
    Route::get('airlines', [AirlinesController::class, 'index']);
    Route::get('airlines/combo', [AirlinesController::class, 'combo']);
    Route::delete('airlines', [AirlinesController::class, 'delete']);
    Route::post('airlines', [AirlinesController::class, 'store']);
    Route::put('airlines', [AirlinesController::class, 'update']);
    
    Route::get('flights', [FlightsController::class, 'index']);
    Route::delete('flights', [FlightsController::class, 'delete']);
    Route::post('flights', [FlightsController::class, 'store']);
    Route::put('flights', [FlightsController::class, 'update']);
});

Route::post('login', [UserController::class, 'authenticate']);
Route::post('register', [UserController::class, 'register']);

