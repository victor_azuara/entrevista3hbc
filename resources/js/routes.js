import PublicLayout from './components/layouts/PublicLayout';
import PrivateLayout from './components/layouts/PrivateLayout';

import Login from './components/pages/Login';
import Dashboard from './components/pages/Dashboard';
import Register from './components/pages/Register';

import Flights from './components/pages/Flights.vue';
import Airlines from './components/pages/Airlines.vue';
import Airports from './components/pages/Airports.vue';


export default [
	{
		path: '',
		component: PublicLayout,
		children: [
			{
				path: '/login',
				name: 'login',
				component: Login
			},
			{
				path: '/register',
				name: 'register',
				component: Register
			}
		]
	},
	{
		path: '/admin',
		component: PrivateLayout,
		children: [
			{
				path: '',
				name: 'dashboard',
				component: Dashboard
			},
			{
				path: '/airports',
				name: 'airports',
				component: Airports
			},
			{
				path: '/airlines',
				name: 'airlines',
				component: Airlines
			},
			{
				path: '/flights',
				name: 'flights',
				component: Flights
			}			
		]
	},
  // Esta se tiene que cambiar por un 404
	{
		path: "*",
		redirect: 'login'
	}
];