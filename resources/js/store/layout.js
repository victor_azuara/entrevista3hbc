export default {
	namespaced: true,
	state: {
		showLoading: false,
		permissions: []
	},
	 mutations: {
		toggleLoading(state, showLoading) {
			state.showLoading = showLoading;
		},
		setPermissions(state, permissions) {
			state.permissions = permissions.map(el => el.name);
		}
  }
}