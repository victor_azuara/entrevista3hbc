require('./bootstrap');

window.Vue = require('vue').default;

import router from './router';
import store from './store/index';
import Vue from 'vue';
import VueToast from 'vue-toast-notification';
import VTooltip from 'v-tooltip';
import Vuelidate from 'vuelidate';
import { ServerTable } from 'vue-tables-2';
import Swal from 'sweetalert2';
import moment from 'vue-moment';
import vSelect from 'vue-select';
// import axios from 'axios';

window.Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

Vue.component("v-select", vSelect);
Vue.use(moment);
Vue.use(Vuelidate);
Vue.use(VTooltip);

Vue.use(ServerTable, {
	sortIcon: {
		base: 'fas',
		is: 'fa-sort',
		up: 'fa-sort-up',
		down: 'fa-sort-down'
	},
	resizableColumns: false
}, false, 'bootstrap4', 'default');

Vue.use(VueToast, {
    position: 'top-right',
	duration: 3000
});

axios.interceptors.request.use(
	(config) => {
		config.headers.Authorization = 'Bearer ' + localStorage.getItem('token');
		return config;
	}
);

const app = new Vue({
    el: '#app',
    router,
    store
});
