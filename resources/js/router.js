import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';

Vue.use(Router);

const router = new Router({
	routes,
	mode: 'history',
});

router.beforeEach((to, from, next) => {


  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('token');

  if (authRequired && !loggedIn) {
    next({name: 'login'});
  } else {
    next();
  }
});

export default router;