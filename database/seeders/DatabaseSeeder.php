<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AirportTableSeeder::class);
        $this->call(AirlineTableSeeder::class);
        $this->call(FlightTableSeeder::class);

        $admin = User::create([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $operator = User::create([
            'name' => 'Operator',
            'email' => 'operator@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $arrayPermsFlights = [    
            'flights.create',
            'flights.read',
            'flights.update',
            'flights.delete',
        ];
        $arrayPermsAirlines = [
            'airlines.create',
            'airlines.read',
            'airlines.update',
            'airlines.delete',
        ];
        $arrayPermsAirports = [
            'airports.create',
            'airports.read',
            'airports.update',
            'airports.delete',
        ];
        
        $resArrayPerms = array_merge($arrayPermsAirlines, $arrayPermsAirports, $arrayPermsFlights);
        
        $permissions = collect($resArrayPerms)->map(function ($permission) {
            return ['name' => $permission, 'guard_name' => 'web'];
        });


        $tmp = [    
            'flights.create',
            'flights.read'
        ];

        Permission::insert($permissions->toArray());

        $roleAdmin = Role::create(['name' => 'admin']);
        $roleOperator = Role::create(['name' => 'operation']);

        $roleAdmin->syncPermissions($arrayPermsFlights);
        $admin->assignRole($roleAdmin);
        
        $roleOperator->syncPermissions($tmp);
        $operator->assignRole($roleOperator);

    }
}
