<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Flight;
use App\Models\Airport;
use App\Models\Airline;

class FlightFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Flight::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->uuid(),
            'type' => $this->faker->randomElement(['PASSENGER', 'FREIGHT']),
            'departure_time' => $this->faker->dateTime()->format('Y-m-d H:m:s'),
            'arrival_time' => $this->faker->dateTime()->format('Y-m-d H:m:s'),
            'departure_id' => Airport::inRandomOrder()->first()->id,
            'destination_id' => Airport::inRandomOrder()->first()->id,
            'airline_id' => Airline::inRandomOrder()->first()->id

        ];
    }
}
