<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Airline;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

use Validator;

class AirlinesController extends Controller {

	public function combo() {
		return Airline::select('id', 'name')->orderBy('name', 'ASC')->get()->toArray();
	}

  public function index(Request $request) {
    $queries = $request->input('query');
    $limit = $request->input('limit');
    $ascending = $request->input('ascending');
    $page = $request->input('page');
    $orderBy = $request->input('orderBy');

    $data = Airline::select('id', 'code', 'name');

    if (isset($queries) && $queries) {
      foreach(json_decode($queries) as $field => $query) {
        $data->where($field, 'LIKE', '%' . $query . '%');
      };
    }

    $count = $data->count();
    
    if (isset($limit) && $limit) {
      $data->limit($limit)->skip($limit * ($page - 1));
    }
        
    if (isset($orderBy)) {
      $direction = $ascending == 1 ? 'ASC' : 'DESC';
      $data->orderBy($orderBy, $direction);
    }

    $results = $data->get()->toArray();

    return [
      'data'	=> $results,
      'count' => $count
    ];
  }

  public function store(Request $request) {
		$code = $request->code;
		$name = $request->name;

		$rules = array(
			"code" => "required|unique:airlines,code,NULL,id,deleted_at,NULL|string",
			"name" => "required|string"
		);

		$validator = Validator::make([
			"code" => $code,
			"name" => $name
		], $rules);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()->all()]);
		}

		try {
			DB::beginTransaction();
			$data = new Airline;
			$data->code = $code;
			$data->name = $name;
      $data->save();

			DB::commit();
		} catch (\PDOException $e) {
			return response()->json(['errors' => $e]);
		}
	}

	public function update(Request $request) {
    $id = $request->id;
		$code = $request->code;
		$name = $request->name;

		$rules = array(
			"id" => "required|integer",
			"code" => "required|unique:airlines,code," . $id . ",id,deleted_at,NULL|string",
			"name" => "required|string"
		);

		$validator = Validator::make([
			"id" => $id,
			"code" => $code,
			"name" => $name
		], $rules);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()->all()]);
		}

		try {
			DB::beginTransaction();
			$data = Airline::find($id);
			$data->code = $code;
			$data->name = $name;
      $data->save();

			DB::commit();
		} catch (\PDOException $e) {
			return response()->json(['errors' => $e]);
		}
	}

	public function delete(Request $request) {

		$id = $request->id;

		$rules = array(
			"id" => "required|integer"
		);

		$validator = Validator::make([
			"id" => $id
		], $rules);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()->all()]);
		}

		try {
			DB::beginTransaction();
			$data = Airline::find($id);
			$data->delete();
			DB::commit();
		} catch (\PDOException $e) {
      DB::rollBack();
			return response()->json(['errors' => $e]);
		}

	}
}
