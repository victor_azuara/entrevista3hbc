<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Flight;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

use Validator;

class FlightsController extends Controller {

  public function index(Request $request) {

		if (!auth()->user()->hasPermissionTo('flights.read')) {
			return response()->json(['errors' => 'No authorization'], 403);
		}

		$queries = $request->input('query');
		$limit = $request->input('limit');
		$ascending = $request->input('ascending');
		$page = $request->input('page');
		$orderBy = $request->input('orderBy');

			$data = Flight::with(['airline', 'departure', 'destination']);

		if (isset($queries) && $queries) {
			foreach(json_decode($queries) as $field => $query) {
				$data->where($field, 'LIKE', '%' . $query . '%');
			};
		}

		$count = $data->count();
		
		if (isset($limit) && $limit) {
						$data->limit($limit)->skip($limit * ($page - 1));
				}
				
		if (isset($orderBy)) {
			$direction = $ascending == 1 ? 'ASC' : 'DESC';
			$data->orderBy($orderBy, $direction);
		}

		$results = $data->get()->toArray();

		return [
			'data'	=> $results,
			'count' => $count
		];
  }

	public function store(Request $request) {
		if (!auth()->user()->hasPermissionTo('flights.store')) {
			return response()->json(['errors' => 'No authorization'], 403);
		}
		$code = $request->code;
		$type = $request->type;
		$airline = $request->airline['id'];
		$departure = $request->departure['id'];
		$destination = $request->destination['id'];
		$departure_time = $request->departure_time;	
		$arrival_time = $request->arrival_time;	

		$rules = array(
			"code" => "required|unique:flights,code,NULL,id,deleted_at,NULL|string",
			"type" => "required|string",
			"airline" => "required|integer",
			"departure" => "required|integer",
			"destination" => "required|integer",
			"departure_time" => "required|string",
			"arrival_time" => "required|string",
		);

		$validator = Validator::make([
			"code" => $code,
			"type" => $type,
			"airline" => $airline,
			"departure" => $departure,
			"destination" => $destination,
			"departure_time" => $departure_time,
			"arrival_time" => $arrival_time
		], $rules);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()->all()]);
		}

		try {
			date_default_timezone_set('America/Tijuana');
			DB::beginTransaction();
			$data = new Flight;
			$data->code = $code;
			$data->type = $type;
			$data->airline_id = $airline;
			$data->departure_id = $departure;
			$data->destination_id = $destination;
			$data->departure_time = date('Y-m-d H:i:s', strtotime($departure_time));
			$data->arrival_time = date('Y-m-d H:i:s', strtotime($arrival_time));
      $data->save();

			DB::commit();
		} catch (\PDOException $e) {
			return response()->json(['errors' => $e]);
		}
	}

	public function update(Request $request) {
		if (!auth()->user()->hasPermissionTo('flights.update')) {
			return response()->json(['errors' => 'No authorization'], 403);
		}
    $id = $request->id;
		$code = $request->code;
		$type = $request->type;
		$airline = $request->airline['id'];
		$departure = $request->departure['id'];
		$destination = $request->destination['id'];
		$departure_time = $request->departure_time;	
		$arrival_time = $request->arrival_time;

		$rules = array(
			"id" => "required|integer",
			"code" => "required|unique:flights,code," . $id . ",id,deleted_at,NULL|string",
			"type" => "required|string",
			"airline" => "required|integer",
			"departure" => "required|integer",
			"destination" => "required|integer",
			"departure_time" => "required|string",
			"arrival_time" => "required|string"
		);

		$validator = Validator::make([
			"id" => $id,
			"code" => $code,
			"type" => $type,
			"airline" => $airline,
			"departure" => $departure,
			"destination" => $destination,
			"departure_time" => $departure_time,
			"arrival_time" => $arrival_time
		], $rules);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()->all()]);
		}

		try {
			DB::beginTransaction();
			$data = Flight::find($id);
			$data->code = $code;
			$data->type = $type;
			$data->airline_id = $airline;
			$data->departure_id = $departure;
			$data->destination_id = $destination;
			$data->departure_time = date('Y-m-d H:i:s', strtotime($departure_time));
			$data->arrival_time = date('Y-m-d H:i:s', strtotime($arrival_time));
      $data->save();

			DB::commit();
		} catch (\PDOException $e) {
			return response()->json(['errors' => $e]);
		}
	}

	public function delete(Request $request) {
		if (!auth()->user()->hasPermissionTo('flights.delete')) {
			return response()->json(['errors' => 'No authorization'], 403);
		}
		$id = $request->id;

		$rules = array(
			"id" => "required|integer"
		);

		$validator = Validator::make([
			"id" => $id
		], $rules);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()->all()]);
		}

		try {
			DB::beginTransaction();
			$data = Flight::find($id);
			$data->delete();
			DB::commit();
		} catch (\PDOException $e) {
			DB::rollBack();
			return response()->json(['errors' => $e]);
		}

	}
}
