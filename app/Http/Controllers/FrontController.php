<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller {
    
    public function index() {
        return view('layouts.app');
    }

    public function userinfo() {
        return response()->json([
            'name' => auth()->user()->name,
            'permissions' => auth()->user()->getAllPermissions()
        ]);
    }
}
