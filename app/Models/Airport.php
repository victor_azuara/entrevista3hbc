<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Airport extends Model
{
    use HasFactory, softDeletes;
    protected $fillable = ['name', 'code', 'city'];

    public function flights() {
        return $this->hasMany(Flight::class);
    }
}
