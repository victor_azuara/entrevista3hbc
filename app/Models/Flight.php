<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Airport;
use App\Models\Airline;
class Flight extends Model {
    use HasFactory, softDeletes;

    protected $fillable = ['code', 'type', 'departure_time', 'arrival_time'];

    public function departure() {
			return $this->belongsTo(Airport::class);
		}

    public function destination() {
			return $this->belongsTo(Airport::class);
		}
    
    public function airline() {
			return $this->belongsTo(Airline::class);
		}
}
